using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CardManager : MonoBehaviour
{
    [System.Serializable]
    public class CardManagerInfo
    {
        public int cardValue;
        public Sprite cardSprite;
    }
    public List<CardManagerInfo> cardList = new List<CardManagerInfo>();
    public List<CardManagerInfo> cardPlayedList = new List<CardManagerInfo>();
    public List<CardManagerInfo> cardPlayedDealerList = new List<CardManagerInfo>();
    public List<Image> hitCardList = new List<Image>();
    public List<Image> hitCardListDealer = new List<Image>();
    [SerializeField] private Image playerStartCard1Image;
    [SerializeField] private Image playerStartCard2Image;
    [SerializeField] private Image dealerStartCard1Image;
    [SerializeField] private Image dealerStartCard2Image;
    [SerializeField] public GameObject victoryScreenBlackJack;
    [SerializeField] public GameObject victoryScreenBlackJackDealer;
    [SerializeField] public GameObject victoryScreen;
    [SerializeField] public GameObject loseScreen;
    [SerializeField] public GameObject DrawScreen;
    [SerializeField] public Button HitButton;
    [SerializeField] public Button StandButton;
    [SerializeField] public Button DealButton;
    [SerializeField] public GameObject RevealCard;
    private int startCard1Index;
    private int startCard2Index;
    private int dealerCard1Index;
    private int dealerCard2Index;
    private int hitCardIndex;
    private int hitCardDealerIndex;
    public int hitcounter;
    public int hitcounterDealer;
    public int PlayedCardAmount = 0;
    int PlayedCardAmountDealer = 0;

    /////////////////////////////////////////////////////////////////////////////////////////////////

    public void onDeal()
    {

        startCard1Index = Random.Range(0, 52);
        startCard2Index = Random.Range(0, 52);
        playerStartCard1Image.sprite = cardList[startCard1Index].cardSprite;
        playerStartCard2Image.sprite = cardList[startCard2Index].cardSprite;

        dealerCard1Index = Random.Range(0, 52);
        dealerCard2Index = Random.Range(0, 52);
        dealerStartCard1Image.sprite = cardList[dealerCard1Index].cardSprite;
        dealerStartCard2Image.sprite = cardList[dealerCard2Index].cardSprite;

        cardPlayedList.Add(cardList[startCard1Index]);
        cardPlayedList.Add(cardList[startCard2Index]);

        cardPlayedDealerList.Add(cardList[dealerCard1Index]);
        cardPlayedDealerList.Add(cardList[dealerCard2Index]);

        CheckIf21();

        DealButton.interactable = false;

    }

    public void onStand()
    {

        CheckIf21Dealer();
        RevealCard.SetActive(false);

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    public void CheckIf21()
    {

        PlayedCardAmount = 0;

        for (var i = 0; i < cardPlayedList.Count; i++)
        {
            if (cardPlayedList[i].cardValue == 1)
            {
                PlayedCardAmount += 11;
            }
            else
            {
                PlayedCardAmount += cardPlayedList[i].cardValue;
            }
        }

       if (PlayedCardAmount > 21)
        {
            for (var i = 0; i < cardPlayedList.Count; i++)
            {
                if (cardPlayedList[i].cardValue == 1)
                {
                    PlayedCardAmount -= 10;
                }
            }
        }

        if (PlayedCardAmount == 21) ///BlackJack
        {

            victoryScreenBlackJack.SetActive(true);
            BetManager.betSystem.BlackJackWin();
            StartCoroutine(RestartLevel());
            
        }
        else if (PlayedCardAmount < 21)
        {

            Debug.Log($" Player Can Hit {PlayedCardAmount}");

        }
        else if (PlayedCardAmount > 21) //Bust
        {

            loseScreen.SetActive(true);
            StartCoroutine(RestartLevel());

        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    public void CheckIf21Dealer()
    {

        PlayedCardAmountDealer = 0;

        for (var i = 0; i < cardPlayedDealerList.Count; i++)
        {

            if (cardPlayedDealerList[i].cardValue == 1)
            {
                PlayedCardAmountDealer += 11;
            }
            else
            {
                PlayedCardAmountDealer += cardPlayedDealerList[i].cardValue;
            }

        }

        if (PlayedCardAmountDealer > 21)
        {
            for (var i = 0; i < cardPlayedDealerList.Count; i++)
            {
                if (cardPlayedDealerList[i].cardValue == 1)
                {
                    PlayedCardAmountDealer -= 10;
                }
            }

            if (PlayedCardAmountDealer > 21)
            {
                
                victoryScreen.SetActive(true);
                BetManager.betSystem.StandardWin();
                StartCoroutine(RestartLevel());
                

            }
        }

        if (PlayedCardAmountDealer == 21)//Dealer BlackJack
        {
            if (cardPlayedList.Count == 2)
            {

                BetManager.betSystem.Draw();
                victoryScreenBlackJackDealer.SetActive(true);
                StartCoroutine(RestartLevel());

            }
            else
            {

                loseScreen.SetActive(true);
                Debug.Log("Dealer BlackJack");
                BetManager.betSystem.DealerBlackJack();
                StartCoroutine(RestartLevel());

            }

        }
        
        if (PlayedCardAmountDealer < 17)
        {

            onHitDealer();
            Debug.Log($" Dealer Can Hit {PlayedCardAmountDealer}");

        }
        else if (PlayedCardAmountDealer > 21)//Dealer Bust
        {

            victoryScreen.SetActive(true);
            StartCoroutine(RestartLevel());

        }
        else if (PlayedCardAmountDealer > 17 && PlayedCardAmountDealer < 21)
        {
            if (PlayedCardAmount > PlayedCardAmountDealer)
        {

                BetManager.betSystem.StandardWin();
                victoryScreen.SetActive(true);
                StartCoroutine(RestartLevel());
                
        }
            else if (PlayedCardAmountDealer < PlayedCardAmount)
        {

                BetManager.betSystem.DealerWin();
                loseScreen.SetActive(true);
                StartCoroutine(RestartLevel());

        }
            else if (PlayedCardAmountDealer == PlayedCardAmount)
        {

                BetManager.betSystem.Draw();
                DrawScreen.SetActive(true);
                StartCoroutine(RestartLevel());

        }
        }
        
    
        StandButton.interactable = false;
        HitButton.interactable = false;
        Debug.Log($" DealerAmount {PlayedCardAmountDealer}");

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    public void onHit()
    {

        hitCardIndex = Random.Range(0, 52);
        hitCardList[hitcounter].sprite = cardList[hitCardIndex].cardSprite;
        cardPlayedList.Add(cardList[hitCardIndex]);
        hitcounter++;

        CheckIf21();

    }

    public void onHitDealer()
    {

        hitCardDealerIndex = Random.Range(0, 52);
        hitCardListDealer[hitcounterDealer].sprite = cardList[hitCardDealerIndex].cardSprite;
        cardPlayedDealerList.Add(cardList[hitCardDealerIndex]);
        hitcounterDealer++;

        CheckIf21Dealer();
    }

    IEnumerator RestartLevel()
    {

        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(0);

    }

}